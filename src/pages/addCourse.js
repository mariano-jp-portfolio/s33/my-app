// Base import
import React, { Fragment, useState } from 'react';

// React-Bootstrap
import { Form, Button } from 'react-bootstrap';

// Export addCourse Page functional component
export default function AddCourse() {
	// States
	const [id, setId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	
	// Effects
	const create = (e) => {
		e.preventDefault();

		setId('');
		setName('');
		setDescription('');
		setPrice(0);
		setStartDate('');
		setEndDate('');
		
		alert('Course successfully created!');
	}
	
	return (
		<Fragment>
			<h3>Add a Course</h3>
			<Form onSubmit={create}>
				<Form.Group>
					<Form.Label>ID</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Course ID" 
						value={id} 
						onChange={(e) => setId(e.target.value)} 
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Name</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Course Name" 
						value={name} 
						onChange={(e) => setName(e.target.value)} 
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Description</Form.Label>
					<Form.Control 
						as="textarea"
						type="text" 
						placeholder="Simple Description" 
						value={description} 
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Price</Form.Label>
					<Form.Control 
						type="number" 
						placeholder="Course Price" 
						value={price} 
						onChange={(e) => setPrice(e.target.value)} 
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Start Date</Form.Label>
					<Form.Control
						type="date"
						value={startDate}
						onChange={(e) => setStartDate(e.target.value)}
						placeholder="Start Date" />
				</Form.Group>
				
				<Form.Group>
					<Form.Label>End Date</Form.Label>
					<Form.Control
						type="date"
						value={endDate}
						onChange={(e) => setEndDate(e.target.value)}
						placeholder="End Date" />
				</Form.Group>
				
				<Button className="btn btn-block" variant="primary" type="submit">Create</Button>
			</Form>
		</Fragment>
	);
}