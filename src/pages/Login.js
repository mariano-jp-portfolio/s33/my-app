// Base import
import React, { Fragment, useState, useEffect, useContext } from 'react';
// Redirect
import { Redirect } from 'react-router-dom';

// React-Bootstrap
import { Form, Button } from 'react-bootstrap';

// Import users data
import usersData from '../data/users';

// UserContext import
import UserContext from '../UserContext';

// Export Login page functional component
export default function Login() {
	// UserContext destructuring
	const { user, setUser } = useContext(UserContext);
	
	// States
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [btnColor, setBtnColor] = useState('primary');
	// Redirection
	const [willRedirect, setWillRedirect] = useState(false);
	
	// Effect
	// Button effects
	useEffect(() => {
		if (email !== '' && password !== '') {
			setBtnColor('primary');
			setIsDisabled(false);
		} else {
			setBtnColor('secondary');
			setIsDisabled(true);
		}
	}, [email, password]);
	
	// Effect that will run whenever any of the user details have changed
	useEffect(() => {
		console.log(`User with an email: ${user.email}, is an admin: ${user.isAdmin}`)
	}, [user.email, user.isAdmin]);
	
	// Authenticating user details
	function authenticate(event) {
		event.preventDefault();
		
		// Authentication based on imported users data
		const match = usersData.find(user => {
			// Data matches the entries from the form
			return (user.email === email && user.password === password);
		})
		
		if (match) {
			// set the details in the user's local storage
			localStorage.setItem('email', email);
			// this will be converted into string once it is set in the localStorage
			localStorage.setItem('isAdmin', match.isAdmin)
			// set the global user state to have props obtained from localStorage
			setUser({
				email: localStorage.getItem('email'),
				isAdmin: match.isAdmin
			});
			
			setWillRedirect(true);
		} else {
			alert('Authentication failed, no match found.');
		}
		
		setEmail('');
		setPassword('');
		
		alert(`Hi ${email}, welcome back!`);
	}
	
	// Use conditional rendering to redirect to /courses if willRedirect is true
	return (
		willRedirect === true ? 
			<Redirect to="/courses" />
			:
			<Fragment>
				<h3>Login</h3>
				<Form onSubmit={authenticate}>
					<Form.Group controlId="formBasicEmail">
						<Form.Label>Email</Form.Label>
						<Form.Control 
							type="email"
							value={email}
							placeholder="Your email"
							onChange={(e) => setEmail(e.target.value)}
							required
						/>
					</Form.Group>
					
					<Form.Group controlId="formBasicPassword">
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password"
							value={password}
							placeholder="Safe word"
							onChange={(e) => setPassword(e.target.value)}
							required
						/>
					</Form.Group>
					
					<Button variant={btnColor} type="submit" disabled={isDisabled}>Login</Button>
				</Form>
			</Fragment>
	);
}