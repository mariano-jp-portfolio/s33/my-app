// Base import
import React, { Fragment, useContext, useState } from 'react';
// Link
import { Link } from 'react-router-dom';

// React-Bootstrap
import { Button, Modal, Table, Row } from 'react-bootstrap';

// UserContext
import UserContext from '../UserContext';

// Database import
import coursesData from '../data/courses';

// Course cards component import
import Course from '../components/Course';

// Export Courses page functional component
export default function Courses() {
	// Destructuring UseContext
	const {user} = useContext(UserContext);
	
	// Disabling a course
	const [show, setShow] = useState(false);
	
	function DisableCourse() {
		setShow(true);
	}
	
	// After disabling or if the user doesn't want to proceed
	function BackToCourses() {
		setShow(false);
	}
	
	// Create multiple Course components corresponding to the content of the coursesData
	const courseUser = coursesData.map((course) => {
		if (course.onOffer) {
			return (
				<Course key={course.id} course={course} />
			);
		} else {
			return null;
		}
	});
	
	// Table rows to be rendered when an admin is logged in
	const courseAdmin = coursesData.map((course) => {
		// returns key-value pairs
		return (
			<tr key={course.id}>
				<td>{course.id}</td>
				<td>{course.name}</td>
				<td>{course.description}</td>
				<td>{course.price}</td>
				<td>{course.onOffer ? 'open' : 'closed'}</td>
				<td>{course.start_date}</td>
				<td>{course.end_date}</td>
				<td>
					<Button as={Link} to="/courses/update" variant="success">Update</Button>
					<Button onClick={DisableCourse} id="disableBtn" variant="danger">Disable</Button>
				</td>
			</tr>
		);
	});
	
	// returns Course prop that is to be used in Course.js component
	return (
		user.isAdmin === true ?
			<Fragment>
				<h1>Courses Dashboard</h1>
				<Table>
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Status</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{courseAdmin}
					</tbody>
				</Table>
				<Modal show={show} onHide={BackToCourses}>
				<Modal.Header closeButton>
					<Modal.Title>Disable this course.</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					Do you really want to disable this course?
				</Modal.Body>
				<Modal.Footer>
					<Button onClick={BackToCourses} variant="secondary">No</Button>
					<Button onClick={BackToCourses} variant="primary">Yes, let's disable it</Button>
				</Modal.Footer>
			</Modal>
			</Fragment>
			:
			<Fragment>
				<Row>
					{courseUser}
				</Row>
			</Fragment>
	);
};