// Base import
import React, { Fragment, useState } from 'react';
// Link
import { Link } from 'react-router-dom';

// React-Bootstrap
import { Form, Button } from 'react-bootstrap';

// Export addCourse Page functional component
export default function UpdateCourse() {
	// States
	// const [id, setId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	
	// Effects
	const update = (e) => {
		e.preventDefault();

		// setId('');
		setName('');
		setDescription('');
		setPrice(0);
		setStartDate('');
		setEndDate('');
		
		alert('Course successfully updated!');
	}
	
	return (
		<Fragment>
			<h3>Update a Course</h3>
			<Form onSubmit={update}>
				{/*<Form.Group>
					<Form.Label>ID</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Course ID" 
						value={id} 
						onChange={(e) => setId(e.target.value)} 
						required
					/>
				</Form.Group>*/}
				
				<Form.Group>
					<Form.Label>Name</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Course Name" 
						value={name} 
						onChange={(e) => setName(e.target.value)} 
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Description</Form.Label>
					<Form.Control 
						as="textarea"
						type="text" 
						placeholder="Simple Description" 
						value={description} 
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Price</Form.Label>
					<Form.Control 
						type="number" 
						placeholder="Course Price" 
						value={price} 
						onChange={(e) => setPrice(e.target.value)} 
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Start Date</Form.Label>
					<Form.Control
						type="date"
						value={startDate}
						onChange={(e) => setStartDate(e.target.value)}
						placeholder="Start Date" />
				</Form.Group>
				
				<Form.Group>
					<Form.Label>End Date</Form.Label>
					<Form.Control
						type="date"
						value={endDate}
						onChange={(e) => setEndDate(e.target.value)}
						placeholder="End Date" />
				</Form.Group>
				
				<Button className="btn btn-block" variant="success" type="submit">Update</Button>
				<Button as={Link} to="/courses" className="btn btn-block" variant="secondary">Go Back</Button>
			</Form>
		</Fragment>
	);
}