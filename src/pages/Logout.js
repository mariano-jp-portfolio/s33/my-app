// Base import
import React, { useEffect, useContext } from 'react';
// Redirect
import { Redirect } from 'react-router-dom';

// UserContext import
import UserContext from '../UserContext';

// Export Logout page functional component
export default function Logout() {
	const { unsetUser, setUser } = useContext(UserContext);
	
	// Clear the local storage of the user's info
	unsetUser();
	
	// Set the user state back to its original value
	setUser({email:null});
	
	// Effect
	useEffect(() => {
		unsetUser();
	})
	
	// Finally, redirect back to Login page
	return (
		<Redirect to='/login' />
	);
}