// Base import
import React, { Fragment, useContext } from 'react';
// Link
import { Link, NavLink } from 'react-router-dom';

// React-Bootstrap
import { Nav, Navbar } from 'react-bootstrap';

// UserContext import
import UserContext from '../UserContext';

// Export Navbar functional component
export default function NavBar() {
	// destructuring UserContext
	const { user } = useContext(UserContext);
	
	return (
		<Navbar expand="lg" bg="dark" variant="dark">
			<Navbar.Brand as={Link} to="/">Sesh33</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
					<Nav.Link as={NavLink} to="/courses" exact>Courses</Nav.Link>
					{(user.isAdmin === true) ?
						<Nav.Link as={NavLink} to="/courses/create">Add a Course</Nav.Link>
						:
						null
					}
				</Nav>
				<Nav>
					{(user.email !== null) ? 
						<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
						:
						<Fragment>
							<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
							<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
						</Fragment>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}